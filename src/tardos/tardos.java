package tardos;

import static java.lang.Math.asin;
import static java.lang.Math.ceil;
import static java.lang.Math.log;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

public class tardos {

    int m, n;
    int[][] x;
    int[] y;
    int col;
    int detect;
    int[] index;

    public tardos(int m, int n, int[][] x, int[] y, int col) {
        this.m = m;
        this.n = n;
        this.x = x;
        this.y = y;
        this.col = col;
        double e1 = Math.pow(10, (-4));
        double Pi = 3.141592;
        double seuil1 = ceil(Pi * col * ceil(log(1 / e1)));
        int iterations = 0;
        double[] p = new double[m];
        while (detect < 1) {
           
            for (int i = 0; i < n; i++) {
                double t = Math.pow(10, -4);
                double tt = asin(sqrt(t));
                for (int j = 0; j < m; j++) {
                    p[i] = Math.pow(sin(tt + (Pi / 2 - 2 * tt) * Math.random()), 2);
                }
            }
            double[][] s = new double[n][m];
            for (int j = 0; j < n; j++) {
                for (int i = 0; i < m; i++) {
                    if (y[i] == 0) {
                        if (x[j][i] == 0) {

                            s[j][i] = sqrt(p[i] / (1 - p[i]));
                        } else {
                            s[j][i] = -sqrt((1 - p[i]) / p[i]);
                        }
                    } else {
                        if (y[i] == 1) {
                            if (x[j][i] == 0) {
                                s[j][i] = -sqrt(p[i] / (1 - p[i]));
                            } else {
                                s[j][i] = sqrt((1 - p[i]) / p[i]);
                            }
                        }
                    }
                }
            }
            double[][] v = s;
            double[] score = sum(v);
            double[] mat_score = score;
            int nombre_colluders = 0;
            index = new int[n];
            for (int j = 0; j < n; j++) {
                if (mat_score[j] > seuil1) {
                    nombre_colluders++;
                    index[j] = j+1;
                }
            }

            int bre_colluders_accuses = nombre_colluders;
            int taux_detection = bre_colluders_accuses / col;
            detect = taux_detection;

            iterations++;

        }
    }

    public int getDetect() {
        return detect;
    }

    public int []  getIndex() {
        return index;
    }

    private double[] sum(double[][] v) {
        int n = v.length;
        int m = v[0].length;
        double[] score = new double[n];
        for (int i = 0; i < n; i++) {
            double s = 0;
            for (int j = 0; j < m; j++) {
                s += v[i][j];
            }
            score[i] = s;
        }
        return score;
    }

}
