package boneh_shaw_primitif;

public class boneh_shaw_primitif {

    int d, ng;
    int[][] u;

    public boneh_shaw_primitif(int ng,int dd) {
        this.ng = ng;

        d = dd;
        int m = (ng - 1) * d;
        u = new int[ng][m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < ng; j++) {
                if (i + 1 > (j) * d) {
                    u[j][i] = 1;
                } else {
                    u[j][i] = 0;
                }
            }
        }
    }

    public int getD() {
        return d;
    }

    public int[][] getU() {
        return u;
    }
}
