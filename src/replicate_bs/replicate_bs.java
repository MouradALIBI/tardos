package replicate_bs;

public class replicate_bs {
    int ng, d;
    int m;
    int[][] rep;
    int[][] u;
    int[] p1;

    public replicate_bs(int ng, int[][] u, int d) {
        this.d = d;
        this.u = u;
        this.ng = ng;
        m = (ng - 1) * d;
        int[] p = new int[m];
        for (int index = 0; index < m; index++) p[index] = index;
        p1 = permut(p);
        rep=new int[ng][m];
        for (int i = 1; i < ng; i++) {
            for (int j = 1; j < m; j++) {
                rep[i][j] = u[i][p1[1]];
            }
        }
    }
    private int[] permut(int[] p) {
        int []perm;
        int temp = 0;
        m = p.length;
        int max = 2147483647;//max= intmax('int32');
        int j = 1;
        for (int i = 0; i < m; i++) {
            j = (int)(Math.random()*m)/(max+1);///j=(*m)/(max+1);
            if (j >= 0) {
                temp = p[j];
                p[j] = p[i];
                p[i] = temp;
            }
        }
        perm = p;
        return perm;
    }

    public int getM() {
        return m;
    }

    public int[][] getRep() {
        return rep;
    }

    public int[] getP1() {
        return p1;
    }
}
