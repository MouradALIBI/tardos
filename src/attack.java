
public class attack {

    int[][] col;
    int[] y_pirate;

    public attack(int[][] col, String typeAttack) {

        this.col = col;
        switch (typeAttack) {
            case "min": {
                y_pirate = majority_attack(col, 0, 1);
                break;
            }
            case "maj": {
                y_pirate = majority_attack(col, 1, 0);
                break;
            }
            case "averaging": {
                y_pirate = averaging_attack(col);
                break;
            }
//            case "wca": {
//                  y_pirate = wca_attack(col);
//                break;
//            }
            case "ran": {
                y_pirate = random_attack(col);
                break;
            }
        }
    }

    public int[] getY_pirate() {
        return y_pirate;
    }

    private int[] majority_attack(int[][] col, int a, int b) {
        int c = col.length;
        int m = col[0].length;
        int[] y = new int[m];
        //////////////////////////////////////////////////////////
        // nombre_one=zeros(1,m);
        int[] nombre_one = new int[m];
        for (int i = 0; i < m; i++) {
            nombre_one[i] = 1;
        }
        // nombre_zero=zeros(1,m);
        int[] nombre_zero = new int[m];
        for (int i = 0; i < m; i++) {
            nombre_one[i] = 0;
        }
        //////////////////////////////////////////////////////////
        for (int j = 0; j < c; j++) {
            for (int i = 0; i < m; i++) {
                if (col[j][i] == 1) {
                    nombre_one[i]++;
                } else {
                    nombre_zero[i]++;
                }
            }
        }

        //y=mode(col);
        for (int i = 0; i < m; i++) {
            if (nombre_one[i] > nombre_zero[i]) {
                y[i] = a;
            } else {
                y[i] = b;
            }
        }

        return y;
    }

    private int[] random_attack(int[][] col) {
        int c = col.length;
        int m = col[0].length; //[c,m]=size (col)
        int[] y = new int[m];
        //////////////////////////////////////////////////////////
        for (int j = 0; j < c - 1; j++) {
            for (int i = 0; i < m; i++) {
                if (col[j][i] == col[j + 1][i]) {
                    y[i] = col[j][i];
                } else {
                    y[i] = (int) (Math.random() * 2);
                }
            }
        }
        return y;
    }

    private int[] averaging_attack(int[][] col) {
        int n = col.length;
        int m = col[0].length; //[c,m]=size (col)
        int[] y = new int[m];
        //////////////////////////////////////////////////////////
        for (int j = 0; j < m; j++) {
            double mean = 0;
            for (int i = 0; i < n; i++) {
                mean+=col[i][j];
            }
            y[j] = (int) Math.round(mean / n);
        }

        return y;
    }
//
//    private int[] wca_attack(int[][] col) {
//          int c = col.length;
//        int m = col[0].length;
//        int[] y = new int[m];
//        int []s=new int[m];
//        for (int j = 0; j < m; j++) {
//            int sum=0;
//            for (int i = 0; i < c; i++) {
//                sum+=col[i][j];
//            }
//            s[j]=sum;
//        }
//        //////////////////////////////////////////////////////////
//        for (int i = 0; i < m; i++) {
//            if(c==1){
//                if(s[i]==0)y[i]=0;
//                else if(s[i]==c-1)y[i]=1;
//                else {
//                    if
//                }
//            }
//        }
//        return y;
//    }
}
