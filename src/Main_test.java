
import accusation2.accusation2;
import boneh_shaw_primitif.boneh_shaw_primitif;
import id_user.id_user;
import replicate_bs.replicate_bs;
import tardos.tardos;

public class Main_test {

    int detect;
    int[] index;

    public int[] getIndex() {
        return index;
    }

    public int getDetect() {
        return detect;
    }

    public  Main_test(int m, int ng, int n, int dd,String typeAttack) {
        
        
        int coll = 5;
        boneh_shaw_primitif bsp = new boneh_shaw_primitif(ng, dd);
        int[][] u = bsp.getU();
        int d = bsp.getD();

        replicate_bs rbs = new replicate_bs(ng, u, d);
        m = rbs.getM();
        int[][] rep = rbs.getRep();
        int p1[] = rbs.getP1();
        id_user idu = new id_user(n, u, d, m, rep);
        int[][] user = idu.getUser();
        int[][] gr_bs = idu.getGr_bs();
        int cc = user[0].length;
        int[][] col = new int[coll][cc];//user(1:5,:);
        for (int i = 0; i < coll; i++) {
            for (int j = 0; j < cc; j++) {
                col[i][j] = user[i][j];
            }
        }
        attack ma = new attack(col, typeAttack);
        int[] y_pirate = ma.getY_pirate();/// pour une 1ere simulation avec 5 pirates et une attaque minoritaire (à changer par la suite avec d'autres attaques)
        int new_rep = ng - 1;
        int new_d = m / new_rep;
        //accusation2 acc2 = new accusation2(new_rep, new_d, m, y_pirate, p1);
        // int score_tri = acc2.getScore_tri();
        // int indice_trie = acc2.getScore_tri();
        // int Y, I = maxi(score_tri);
        int I = 1;// score_tri matrix de un seul line [1,N] donc I = 1 ; tell que soit ;   
        //int x = 0;//user(I*(1:n/ng),:);
        int a = (int) (n / ng);
        int b = user[0].length;
        int[][] x = new int[a][b];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                x[i][j] = user[i][j];
            }
        }
        tardos tardos = new tardos(m, a, x, y_pirate, coll);
        detect = tardos.getDetect();
        index = tardos.getIndex();
        
    }

}
