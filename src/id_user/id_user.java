package id_user;

public class id_user {

    int n, d, m;
    int[][] u;
    int [][] user;
     int[][] gr_bs;

    public id_user(int n, int[][] u, int d, int m, int[][] rep) {
        this.n = n;
        this.u = u;
        this.d = d;
        this.m = m;
        int[][] user_t = new int[n][m];
         
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                user_t[i][j] = (int) (Math.random() * 1);
            }
        }
        int[][] id = unique(user_t);///unique(user_t, 'rows');
        int ng = (int) ((m / d) + 1);///round ((m/d)+1)
        int n_user = (int) (n / ng);//round(n/ng);
       int k = 0;// gr_bs =[gr_bs;repmat(rep(k,:),n_user,1)];
       gr_bs=new int[ng*n_user][m];
            for (int i = 0; i < ng; i++) {
                for (int j = 0; j < n_user; j++) {
                    for (int l = 0; l < m; l++) {
                         gr_bs[k][l]=rep[i][l];
                    }
                    k++;
                }
        }
            int c=ng*n_user;
        user=new int[c][2*m];
        for (int i = 0; i < c; i++) {
            for (int j = 0; j < m; j++) {
                user[i][j]=gr_bs[i][j];
            }
            for (int j = 0; j < m; j++) {
                user[i][m+j]=user_t[i][j];
            }
        }
    }

    public int [][] getGr_bs() {
        return gr_bs;
    }

    public int[][] getUser() {
        return user;
    }

    /*b = unique(a); */ private /*static*/ int[][] unique(int[][] a) {
        int N = a.length;
        int M = a[0].length;
        int[][] b = new int[1][M];
        for (int j = 0; j < M; j++) {
            b[0][j] = a[0][j];
        }
        for (int i = 1; i < N; i++) {
            int[] line = new int[M];
            line = a[i];
            boolean existe = isExiste(line, b);
            if (!existe) {
                b = addLine(line, b);
            }
        }
        return b;
    }

    private /*static*/ boolean isExiste(int[] line, int[][] b) {

        for (int i = 0; i < b.length; i++) {
            if (isEqual(line, b[i])) {
                return true;
            }
        }
        return false;
    }

    private /*static*/ boolean isEqual(int[] lineA, int[] lineB) {
        for (int i = 0; i < lineA.length; i++) {
            if (lineA[i] != lineB[i]) {
                return false;
            }
        }
        return true;
    }

    private /*static*/ int[][] addLine(int[] line, int[][] b) {
        System.out.println("ON AJOUT ICI !!!");
        int[][] newB = new int[b.length + 1][b[0].length];
        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < b[0].length; j++) {
                newB[i][j] = b[i][j];
            }
        }
        for (int j = 0; j < b[0].length; j++) {
            newB[b.length][j] = line[j];
        }

        return newB;
    }

}
